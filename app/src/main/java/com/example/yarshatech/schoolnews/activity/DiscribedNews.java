package com.example.yarshatech.schoolnews.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yarshatech.schoolnews.DTO.Value;
import com.example.yarshatech.schoolnews.R;
import com.example.yarshatech.schoolnews.utills.Constants;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class DiscribedNews extends AppCompatActivity {

    private TextView newsTitleTV;
    private TextView createdDate, publishedDateTV, newsDiscription;
    private ImageView newsImageIV;
    private ImageView backIC;
    private Value value;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discribed_news);
        initialiseViews();
        setOnClickListners();
        if (getIntent().getExtras() != null) {
            value = (Value) getIntent().getExtras().get(Constants.Bundle_key.NEWS_DETAILS);
            configureNewsDescription();
        }
    }

    private void configureNewsDescription() {

        Document document = Jsoup.parse(value.getDescription());
        Elements element = document.select("Description");

        newsTitleTV.setText(value.getTitle());
        createdDate.setText(value.getCreatedDate());
        publishedDateTV.setText(value.getPublishDate());
        newsDiscription.setText(document.text());
    }

    private void setOnClickListners() {
        backIC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initialiseViews() {
        newsTitleTV = findViewById(R.id.newsTitleTV);
        createdDate = findViewById(R.id.CreatedDateTV);
        publishedDateTV = findViewById(R.id.publishedDateTV);
        newsDiscription = findViewById(R.id.newsDiscription);
        newsImageIV = findViewById(R.id.newsImageIV);
        backIC=findViewById(R.id.backIC);
    }


}
