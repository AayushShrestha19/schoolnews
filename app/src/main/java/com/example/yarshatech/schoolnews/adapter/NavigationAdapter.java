package com.example.yarshatech.schoolnews.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yarshatech.schoolnews.R;
import com.example.yarshatech.schoolnews.model.NavigationItem;

import java.util.List;

public class NavigationAdapter extends BaseAdapter {

    private Context context;
    private List<NavigationItem> navDatalist;

    public NavigationAdapter(Context context, List<NavigationItem> navDatalist) {
        this.context = context;
        this.navDatalist = navDatalist;
    }



    @Override
    public int getCount() {
        return navDatalist.size();
    }

    @Override
    public Object getItem(int position) {
        return navDatalist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView==null){
            convertView = layoutInflater.inflate(R.layout.activity_navdata_list_item,null,false);
        }
        ImageView navImageView = convertView.findViewById(R.id.navImageIV);
        TextView navTitle = convertView.findViewById(R.id.navTitleTV);

        NavigationItem navigationItem = navDatalist.get(position);
        navTitle.setText(navigationItem.getNavTitleTV());
        navImageView.setImageResource(navigationItem.getNavIV());
        navImageView.setColorFilter(R.color.colorPrimary);
        return convertView;
    }
}
