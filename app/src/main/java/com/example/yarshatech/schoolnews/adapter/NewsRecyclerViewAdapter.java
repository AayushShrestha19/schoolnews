package com.example.yarshatech.schoolnews.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.yarshatech.schoolnews.DTO.Value;
import com.example.yarshatech.schoolnews.R;
import com.example.yarshatech.schoolnews.activity.DiscribedNews;
import com.example.yarshatech.schoolnews.fragments.DiscribedNewsFragment;
import com.example.yarshatech.schoolnews.utills.Constants;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.Serializable;
import java.util.List;

public class NewsRecyclerViewAdapter extends RecyclerView.Adapter<NewsRecyclerViewAdapter.NewsViewHolder> {
    private Context context;
    private List<Value> newsList;
    private OnItemClickListner mlistner;

    public interface OnItemClickListner{
        void onItemClick(int position);
    }


 public void setOnItemClickListner(OnItemClickListner listner){
       mlistner=listner;
 }

    public interface HomeFragmentListner {
        void OnInput(List<Value> input);
    }


    public NewsRecyclerViewAdapter(Context context, List<Value> newsList) {
        this.context = context;
        this.newsList = newsList;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.main_news_views, viewGroup, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder newsViewHolder, int position) {
         final Value value = newsList.get(position);
        newsViewHolder.shortDiscriptionTV.setText(value.getShortDescription());
        newsViewHolder.createdDateTV.setText(value.getCreatedDate());
        newsViewHolder.newsTitle.setText(value.getTitle());
//        Glide.with(context).load(value.getImage()).into(newsViewHolder.newsImageIV);





        newsViewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newsDetail = new Intent(context, DiscribedNews.class);
                newsDetail.putExtra(Constants.Bundle_key.NEWS_DETAILS, value);
                context.startActivity(newsDetail);

                DiscribedNewsFragment  discribedNewsFragment = new DiscribedNewsFragment();
                Bundle bundle=new Bundle();
                bundle.putString("newsTitle" ,value.getTitle());
                bundle.putString("createdDate",value.getCreatedDate());
                bundle.putString("discription",value.getDescription());
                bundle.putString("publishedDate",value.getPublishDate());
                discribedNewsFragment.setArguments(bundle);




            }
        });


    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {
        private TextView newsTitle;
        private TextView createdDateTV;
        private TextView shortDiscriptionTV;
        private ImageView newsImageIV;
        private RelativeLayout parentLayout,fragcontainer;

        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);
            newsTitle = itemView.findViewById(R.id.newsTitleTV);
            createdDateTV = itemView.findViewById(R.id.createdDateTV);
            shortDiscriptionTV = itemView.findViewById(R.id.shortDiscriptionTV);
            newsImageIV = itemView.findViewById(R.id.newsImageIV);
            parentLayout = itemView.findViewById(R.id.parentLayout);
            fragcontainer=itemView.findViewById(R.id.fragcontainer);


        }
    }

   /* @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if (recyclerView instanceof HomeFragmentListner) {
            listner = (HomeFragmentListner) context;
        } else {
            throw new RuntimeException(context.toString() + "must implement fragmentListner");
        }
    }*/
}
