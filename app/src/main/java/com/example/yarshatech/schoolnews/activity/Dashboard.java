package com.example.yarshatech.schoolnews.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.example.yarshatech.schoolnews.DTO.Value;
import com.example.yarshatech.schoolnews.R;
import com.example.yarshatech.schoolnews.adapter.NavigationAdapter;
import com.example.yarshatech.schoolnews.fragments.DiscribedNewsFragment;
import com.example.yarshatech.schoolnews.fragments.HomeFragment;
import com.example.yarshatech.schoolnews.interfaces.IOnBackPressed;
import com.example.yarshatech.schoolnews.model.NavigationItem;

import java.util.ArrayList;
import java.util.List;

public class Dashboard extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private RelativeLayout navViewRV;
    private ListView draweritemList;
    private android.support.v7.widget.Toolbar toolbar;
    private ImageView searchIC;
    private String TAG = "Fragments";
    private List<Value> newsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        initializeViews();
        configureNavigationDrawer();


    }

    private void initializeViews() {
        drawerLayout = findViewById(R.id.drawerLayout);
        navViewRV = findViewById(R.id.navViewRV);
        draweritemList = findViewById(R.id.draweritemList);
        toolbar = findViewById(R.id.toolbarRV);
        searchIC = findViewById(R.id.toobarsearchIC);

    }

    private void configureNavigationDrawer() {
        configureWidth();
        configureDrawerLayout();
        configureNavigationList();
        ReplaceFragment(new HomeFragment());


    }

    private void configureNavigationList() {
        List<NavigationItem> navigationItemList = new ArrayList<>();

        NavigationItem navMenu = new NavigationItem();
        navMenu.setNavTitleTV("Home");
        navMenu.setNavIV(R.drawable.home_ic);
        navigationItemList.add(navMenu);

        NavigationItem navoffers = new NavigationItem();
        navoffers.setNavTitleTV("About Us");
        navoffers.setNavIV(R.drawable.about_us);
        navigationItemList.add(navoffers);


        NavigationAdapter adapter = new NavigationAdapter(this, navigationItemList);
        draweritemList.setAdapter(adapter);


        draweritemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        ReplaceFragment(new HomeFragment());
                        closeDrawer();
                        break;
                    case 1:
                        ReplaceFragment(new DiscribedNewsFragment());
                        closeDrawer();
                        break;


                }
            }
        });

    }

    private void closeDrawer() {
        drawerLayout.closeDrawer(Gravity.START);
    }


    private void configureWidth() {
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.7);
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) navViewRV.getLayoutParams();
        params.width = width;
        navViewRV.setLayoutParams(params);

    }

    private void configureDrawerLayout() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    private void ReplaceFragment(Fragment fragment) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentTransaction.replace(R.id.fragmentcontainerRV, fragment);
        fragmentTransaction.addToBackStack(TAG);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentcontainerRV);
        if (!(fragment instanceof IOnBackPressed) || !((IOnBackPressed) fragment).onBackPressed()) {
            super.onBackPressed();
        }
    }


}
