package com.example.yarshatech.schoolnews.support;

import com.example.yarshatech.schoolnews.DTO.News;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;



public class ApiHandler {

    public static final String BASE_URL = "http://school3.yarshatech.com/api/schooldata/NewsAnnouncements/";

    public static final String menus = "/api/schooldata/NewsAnnouncements?fbclid=IwAR3LgzFLgahlkuajDVdDCki8h6cLBvukAUX4jZ2fOAa1OoWN2n5LjjauLSU";

    public static final String menu_list = "/wp-json/codelogic/v2/posts/?catid=";
    public static final String Key="$orderby=PublishDate";


    public static PostService postService=null;
    public static PostService getService(){
        if (postService==null){
           Retrofit retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
           postService = retrofit.create(PostService.class);
        }
        return postService;
    }
    public interface PostService{
        @GET("?"+Key)
        Call<News> getNewsList();

    }


}

