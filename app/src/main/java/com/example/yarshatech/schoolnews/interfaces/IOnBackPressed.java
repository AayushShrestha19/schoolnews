package com.example.yarshatech.schoolnews.interfaces;

public interface IOnBackPressed {
    boolean onBackPressed();
}
