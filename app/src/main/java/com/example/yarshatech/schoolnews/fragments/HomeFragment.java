package com.example.yarshatech.schoolnews.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yarshatech.schoolnews.DTO.News;
import com.example.yarshatech.schoolnews.DTO.Value;
import com.example.yarshatech.schoolnews.R;
import com.example.yarshatech.schoolnews.adapter.NewsRecyclerViewAdapter;
import com.example.yarshatech.schoolnews.support.ApiHandler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment {

    private RecyclerView newsRecyclerView;
    TextView todaysDate;
    private ProgressDialog progressDialog;
    private List<Value> mlist = new ArrayList<>();
    View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);

        initialiseViews();
        configureNewsRecyclerView();
//        configureProgressDialog();


        return view;


    }

    private void configureProgressDialog() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);
    }

    private void configureNewsRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        newsRecyclerView.setLayoutManager(linearLayoutManager);
        String date_n = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()).format(new Date());
        todaysDate.setText(date_n);
        getDataFromServer();

    }


    private void initialiseViews() {
        newsRecyclerView = view.findViewById(R.id.newsRecyclerViewRV);
        todaysDate = view.findViewById(R.id.todaysDate);
    }


    private void getDataFromServer() {

        final retrofit2.Call<News> newsList = ApiHandler.getService().getNewsList();
        newsList.enqueue(new Callback<News>() {
            @Override
            public void onResponse(retrofit2.Call<News> call, Response<News> response) {

                News list = response.body();

                Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
                Toast.makeText(getContext(), "List::" + list.getValue().size(), Toast.LENGTH_SHORT).show();

                NewsRecyclerViewAdapter adapter =new NewsRecyclerViewAdapter(getContext(), list.getValue());
                newsRecyclerView.setAdapter(adapter);


            }

            @Override
            public void onFailure(retrofit2.Call<News> call, Throwable t) {

                Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();

            }
        });


    }


  /*  private void ReplaceFragment(Fragment fragment) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentTransaction.replace(R.id.fragmentcontainerRV, fragment);
        fragmentTransaction.addToBackStack(TAG);
        fragmentTransaction.commit();
    }*/


}
