package com.example.yarshatech.schoolnews.model;

public class NavigationItem {
    private String navTitleTV;
    private int navIV;

    public String getNavTitleTV() {
        return navTitleTV;
    }

    public void setNavTitleTV(String navTitleTV) {
        this.navTitleTV = navTitleTV;
    }

    public int getNavIV() {
        return navIV;
    }

    public void setNavIV(int navIV) {
        this.navIV = navIV;
    }







}
