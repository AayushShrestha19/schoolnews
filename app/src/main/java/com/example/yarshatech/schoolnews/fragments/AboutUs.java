package com.example.yarshatech.schoolnews.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yarshatech.schoolnews.R;
import com.example.yarshatech.schoolnews.interfaces.IOnBackPressed;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUs extends Fragment implements IOnBackPressed {


    public AboutUs() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about_us, container, false);


    }

    @Override
    public boolean onBackPressed() {
        return true;
    }
}
