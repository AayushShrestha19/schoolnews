package com.example.yarshatech.schoolnews.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yarshatech.schoolnews.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiscribedNewsFragment extends Fragment {
    private TextView newsTitleTV;
    private TextView createdDateTV, publishedDateTV, newsDiscription;
    private ImageView newsImageIV;


    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_discribed_news, container, false);
        initialiseViews();

        Bundle arguments = getArguments();
        if (arguments!=null){
            String newsTitle= arguments.get("newsTitle").toString();
            String createdDate= arguments.get("createdDate").toString();
            String discription= arguments.get("discription").toString();
            String publishedDate= arguments.get("publishedDate").toString();
            newsTitleTV.setText(newsTitle);
            createdDateTV.setText(createdDate);
            newsDiscription.setText(discription);
            publishedDateTV.setText(publishedDate);
        }
        return view;
    }

    private void initialiseViews() {
        newsTitleTV = view.findViewById(R.id.newsTitleTV);
        createdDateTV = view.findViewById(R.id.createdDateTV);
        publishedDateTV = view.findViewById(R.id.publishedDateTV);
        newsDiscription = view.findViewById(R.id.newsDiscription);
        newsImageIV = view.findViewById(R.id.newsImageIV);
    }

}
